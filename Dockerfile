FROM debian

WORKDIR /app

RUN apt-get update && apt-get install -y \
    graphviz \
    m4 \
    python3-full \
    && rm -rf /var/lib/apt/lists/*
