define(`_std_color',`#f3f3f3')dnl
define(`_dom_color',`crimson')dnl
define(`_sub_color',`deeppink')dnl
define(`_item_color',`#133b19')dnl
define(`_mcs1_color',`#12354c')dnl
define(`_mcs2_color',`#1b3f43')dnl
define(`_mcs3_color',`#451742')dnl
define(`_mcsx_color',`#472014')dnl
define(`_diana_color',`#112211')dnl
define(`_john_color',`#111133')dnl
define(`_kagney_color',`#554400')dnl
define(`_mom_color',`#483d8b')dnl
define(`_penny_color',`#331111')dnl
define(`_sister_color',`#554400')dnl
define(`_sophie_color',`#31314f')dnl
define(`_tasha_color',`#332200')dnl
define(`_undefined_color',`#636363')dnl
define(`_tf_color',`gold')dnl
define(`_std_link',``color='_std_color')dnl
define(`_dom_link',``color='_dom_color')dnl
define(`_sub_link',``color='_sub_color')dnl
define(`_std_or_sub_link',``color="'_std_color'`:'_sub_color`"')dnl
define(`_tf_link',``color='_tf_color')dnl
define(`_lf_left',`<br align="left"/>')dnl
define(`_love_link',`color=pink')dnl
define(`_below',`arrowhead=none')dnl
define(`_newline',``$1<br align="left"/>'ifelse(`$#',`1',`',`_newline(shift($@))')')dnl
define(`_pp',`ifelse(`$#',`0',,``$1'`<br align="left"/>'_newline(shift($@))')')dnl
define(`_rtrim',`patsubst(`$@',` *$')')dnl
define(`_ltrim',`patsubst(`$@',`^ *\(.*\)$',`\1')')dnl
define(`_trim',`_rtrim(_ltrim($@))')dnl
define(`_dosest',`$1 [label=<<table border="0"><tr><td bgcolor="$2" align="left"><b>$3</b></td></tr><tr><td fixedsize="true" width="140" height="80" align="left" valign="top"><font point-size="9"><b><i>$4</i></b><br align="left"/><br align="left"/>$5</font></td></tr></table>>')dnl
define(`_notest',`$1 [label=<<table border="0"><tr><td align="left"><font color="$2"><b>$3</b></font></td></tr><tr><td fixedsize="true" width="140" height="80" align="left" valign="top"><font point-size="9">$4</font></td></tr></table>>')dnl
define(`_person_label_with_color',`<<font color="_std_color()"><table border="0" cellspacing="0" cellpadding="0" bgcolor="$3"><th><td><b>$1</b></td></th><tr><td fixedsize="true" width="140" height="100"><img scale="both" src="$2"/></td></tr></table></font>>')dnl
define(`_person_label_simple',`<<font color="_std_color()"><table border="0"><th><td><b>$1</b></td></th><tr><td fixedsize="true" width="140" height="100"><img scale="both" src="$2"/></td></tr></table></font>>')dnl
define(`_person_label',`ifelse(`$#',`3',`_person_label_with_color(`$1',`$2',`$3')',`_person_label_simple(`$1',`$2')')')dnl
define(`_person',`$1 [label=_person_label($1,$2)]')dnl
define(`_mcs1',`_dosest(_trim(`$1'),`_mcs1_color',_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_mcs2',`_dosest(_trim(`$1'),`_mcs2_color',_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_mcs3',`_dosest(_trim(`$1'),`_mcs3_color',_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_mcsx',`_dosest(_trim(`$1'),`_mcsx_color',_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_mcs_undefined',`_dosest(_trim(`$1'),`_undefined_color',_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_item',`_dosest(_trim(`$1'),`_item_color',_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_req',`_notest(_trim(`$1'),_trim(`$2'),_trim(`$3'),_trim(`$4'))]')dnl
define(`_depends_on',``arrowhead=vee,style=dotted,label='ifelse(`$#',`0',`"depends on"',`"$1"')')dnl
define(`_depends_on_back',``arrowhead=vee,dir=back,style=dotted,label='ifelse(`$#',`0',`"depends on"',`"$1"')')dnl
define(`_lead_to',``arrowhead=vee,style=dashed,label='ifelse(`$#',`0',`"leads to"',`"$1"')')dnl
define(`_lead_to_back',``arrowtail=vee,dir=back,style=dashed,label='ifelse(`$#',`0',`"leads to"',`"$1"')')dnl
dnl
digraph TheCompany {
    graph [
        bgcolor="#1f2121"
        fontname="Helvetica,sans-serif"
        labeljust=r
        labelloc=b
        rankdir=LR
    ]

    node [
        style=filled
        shape=rect
        fontname="Helvetica,sans-serif"
        shape=plaintext
        fillcolor="#0e0e0e"
        fontcolor="_std_color"
    ]

    edge [
        fontname="Helvetica,sans-serif"
        fontcolor="_std_color"
        color="_std_color"
    ]
    compound=true

    subgraph cluster_Diana {
        penwidth=5
        color="_diana_color"
        label=_person_label(
            Diana,
            img/diana.png,
            _diana_color
        )

        _person(Diana,img/diana.png)

        {
            node [group="diana_tamed"]
            _mcs1(DianaInfo,
                Info,
                _pp(Have her start talking to you,more about Sophie),
                _pp(• Add Diana convo event 13,• Can learn more about The,Company background))
            _mcs1(DianaPersonal,
                Personal,
                _pp(Have her start talking to you,more about herself),
                _pp(• Add work event 64,• Add Diana convo event 16))
            _mcs2(DianaMorningGreet,
                Morning greet,
                _pp(Have her start greeting you in,the morning with a kiss),
                _pp(• Add work event 65,• Add Diana convo event 18))
            _mcs2(DianaTaming,
                Taming,
                _pp(Have her start trusting you to,be more dominant with her),
                _pp(• Add Diana convo event 19))
            _mcs3(DianaCommitted,
                Committed,
                _pp(Have her submit to you as a,committed pet),
                _pp(• Add Diana convo event 24,`• Initialize DianaSub to 1 (this',`value varies with MC decisions',`regarding Diana)'))
            _mcs3(DianaCollar_Dom,
                Collar,
                _pp(Attempt to fortify the previous,dose),
                _pp(• Add work event 75,• Add diana convo 25))
        }

        {
            node [group="diana_neutral"]
            _mcs1(DianaFlirty,
                Flirty,
                _pp(Have her start acting even,more flirty with you),
                _pp(• Add Diana convo event 11))
            _mcs2(DianaSex,
                Sex,
                _pp(Have her want to start having,sex in her office),
                _pp(• Add Diana convo event 17))
            _mcsx(DianaWolf,
                Wolf,
                _pp(Give Diana a dose of MCS-X),
                _pp(<font color="_tf_color">• Diana &rarr; Wolf TF</font>,• Text only))
        }

        {
            node [group="diana_dom"]
            _mcs1(DianaOral,
                Oral,
                _pp(Have her start going down on,you in her office),
                _pp(• Add work events 60~61,• Add Diana convo event 12))
            _mcs1(DianaSexy,
                Sexy,
                _pp(ave her start dressing and,acting more promiscuously,around the office),
                _pp(• Add work events 62 and 64,• Add Diana convo event 15))
            _mcs2(DianaPet,
                Pet,
                _pp(Have her show interest in,taking a pet),
                _pp(• Add work event 66,• Add Diana convo event 20))
            _mcs2(DianaMistress,
                Mistress,
                _pp(Have her want to take her,dominant tendencies further),
                _pp(• Add work event 68,• Add Diana convo event 21))
            _mcs3(DianaNTR,
                NTR,
                _pp(Have her start wanting to take a,lover),
                _pp(• Add work event 71~73,• Add Diana convo event 23))
            _mcs3(DianaCollar_Sub,
                Collar,
                _pp(Attempt to fortify the previous,dose),
                _pp(`• Add work events 66, 68, 71~74',• Add diana convo 23 and 25,• Remove diana convo 24))
        }

        Diana -> DianaFlirty
        DianaFlirty -> DianaInfo
        DianaFlirty -> DianaOral
        DianaInfo -> DianaPersonal
        DianaOral -> DianaSexy
        DianaPersonal -> DianaSex
        DianaSexy -> DianaSex
        DianaSex -> DianaMorningGreet
        DianaSex -> DianaPet
        DianaMorningGreet -> DianaTaming [_dom_link]
        DianaPet -> DianaMistress [_sub_link]
        DianaMistress -> DianaNTR [_sub_link]
        DianaTaming -> DianaCommitted [_dom_link]
        DianaCommitted -> DianaCollar_Dom [_dom_link,label="forced if: DianaSub > 0"]
        DianaCommitted -> DianaCollar_Sub [_sub_link,label=otherwise]
        DianaNTR -> DianaCollar_Sub [_sub_link]
        DianaCollar_Dom -> DianaWolf [_dom_link]
        DianaCollar_Sub -> DianaWolf [_sub_link]
    }

    subgraph cluster_John {
        penwidth=5
        color="_john_color"
        label=_person_label(
            John,
            img/john.png,
            _john_color
        )

        _person(John,img/john.png)

        {
            node [group="john_be_bad"]
            _req(JohnBackfireReq,
                _sub_color,
                Unless...,
                _pp(• MC is submissive and has,sucked John,• John is not be dosed yet,• Tasha hasn&apos;t quit and isn&apos;t,`TFed (see her path for the',`conditions for that one)'))
            _mcs1(JohnBackfire,
                Backfire,
                _pp(Open John&apos;s bottle and slip,some serum into it),
                _pp(• John becomes your dom,• He tries to dominate Penny,`• If he succeeds, it is a slow burn',to becoming a perma bimbo))
            _req(JohnPermaBimboTFReq,
                _sub_color,
                Requirements,
                _pp(• John dominating all women,• Being bullied by Tasha for some,time,• Rejecting Saya&apos;s one time offer,after Tasha doses you))
            _req(JohnPermaBimboTF,
                _sub_color,
                `MC &rarr; Bimbo TF',
                _pp(<b>Permanent</b>,,• Lock physical traits,• Lock identity traits,• Lock haircuts,• Lock willpower,• Lock arousal,• Lock name))
        }

        {
            node [group="john_lab"]
            _mcs1(JohnLabHelp,
                Lab helper,
                _pp(Have him come to the lab for,some testing),
                _pp())
            _mcs2(JohnLabTest,
                Lab test,
                _pp(Have him come to the lab,for some testing),
                _pp(• Change Tasha morning routine,• Text changes overall))
            _mcs2(JohnOralBathroom,
                Oral bathroom,
                _pp(Have him start orally servicing,people in the bathroom),
                _pp(Can decide partners:,• Only females,• 50/50 females/males))
            _req(JohnChanelTF,
                _tf_color,
                `John &rarr; Chanel TF',
                _pp(Requires:,• witnessing bathroom oral event,• 3 or more lab testing sessions,on evenings))
        }

        {
            node [group="john_revenge"]
            _mcs2(JohnNumb,
                Numb,
                _pp(`Modify John&apos;s body, making',him permanently erect and,unable to feel pleasure),
                _pp())
            _mcs2(JohnOralSlave,
                Oral slave,
                _pp(Turn John&apos;s throat into his,only erogenous zone),
                _pp(• Add work events 29~30,• Can get relief from John in the,bathroom))
        }

        {
            node [group="john_docile"]
            _mcs1(JohnCompliant,
                Compliant,
                _pp(Make John a little more,agreeable),
                _pp(• Remove work events 19 and 21))
            _mcs1(JohnNice,
                Respect you,
                _pp(Try to make John less abrasive),
                _pp(• Add John convo 8))
            _mcs1(JohnNiceTasha,
                Respect coworkers,
                _pp(Have John start to treat his,coworkers better),
                _pp(• Add several convos...,• Increase lab material yield))
            _mcs2(JohnSubmission,
                Submission,
                _pp(Make John submissive and,meek),
                _pp(• Add work event 31,• Can witness John being bullied,in the bathroom))
            _mcs2(JohnBitch,
                Bitch,
                _pp(Turn John into your personal,bitch),
                _pp(• Not implemented yet))
        }

        {
            node [group="john_love"]
            _mcs2(JohnFriend,
                Friend,
                _pp(Make John convinced you&apos;re,his best friend),
                _pp(• Add work events 32~33))
            _mcs2(JohnLover,
                Lover,
                _pp(Make John completely devoted,to you),
                _pp(• Enable the dating flag,• As of current version it seems to,be permanent))
        }

        John -> JohnBackfireReq [_sub_link,_below]
        JohnBackfireReq -> JohnBackfire [_sub_link]
        John -> JohnCompliant [label="otherwise"]
        JohnBackfire -> JohnPermaBimboTFReq [_sub_link,_lead_to(),_below]
        JohnCompliant -> JohnLabHelp
        JohnCompliant -> JohnNice
        JohnNice -> JohnNiceTasha
        JohnPermaBimboTFReq -> JohnPermaBimboTF [_sub_link,_lead_to]
        JohnLabHelp -> JohnLabTest
        JohnLabHelp -> JohnNumb
        JohnNiceTasha -> JohnFriend [_love_link]
        JohnNiceTasha -> JohnSubmission
        JohnFriend -> JohnLover [_love_link]
        JohnLabTest -> JohnOralBathroom
        JohnNumb -> JohnOralSlave
        JohnSubmission -> JohnBitch
        JohnOralBathroom -> JohnChanelTF [_tf_link,_lead_to]

        // Avoid edge crossing with tasha requiring oral slave
        rank=same { JohnCompliant -> JohnBackfireReq [style=invis] }
    }

    subgraph cluster_Kagney {
        penwidth=5
        color="_kagney_color"
        label=_person_label(
            Kagney,
            img/kagney.png,
            _kagney_color
        )

        _person(Kagney,img/kagney.png)
        _mcs1(KagneySlutty,
            Slutty,
            _pp(Have her start behaving more,promiscuously at work)
            _pp())
        _mcs1(KagneyFuck,
            Fuck,
            _pp(Have her desperately want to,fuck you all day),
            _pp())
        _mcs2(KagneyLabFuck,
            Lab fuck,
            _pp(Have her start coming to the,lab at night for sex),
            _pp())
        _mcs_undefined(KagneyBimbo,
            Bimbo,
            _pp(Use the BMB1 on Kagney),
            _pp(• Require to have a BMB1 in,inventory,• Work event 41 is enabled when,MC has a penis))

        Kagney -> KagneySlutty
        KagneySlutty -> KagneyFuck
        KagneyFuck -> KagneyLabFuck
        KagneyLabFuck -> KagneyBimbo
    }

    subgraph cluster_Mom {
        penwidth=5
        color="_mom_color"
        label=_person_label(
            Mom,
            img/mom.png,
            _mom_color
        )

        _person(Mom,img/mom.png)
        _mcs1(MomCoffee,
            Coffee,
            _pp(Have her bring you coffee,every morning),
            _pp())

        {
            node [group="mom_collar"]
            _req(MomGiveCollarReq,
                `_std_color',
                `Attempt to put the<br align="left"/>cat collar on mom<br align="left"/>',
                _pp(• Require to buy cat collar,• Visit mom in kitchen,• Not possible if MC is already,wearing a cat collar))
            _req(MomGiveCollarBackfire,
                _sub_color,
                MC &rarr; Cat collar TF,
                _pp(<b>Permanent</b>,,Regularly:,• Lose dominance,• Shrink penis,• Increase size of vagina and ass))
        }

        {
            node [group="mom_kiss"]
            _mcs1(MomKiss,
                Kiss,
                _pp(Have her give you a kiss in the,morning along with your coffee),
                _pp(• Add home events 6~7,• Add mom convos 4 and 6))
            _mcs1(MomUndress,
                Undress,
                _pp(Have her start behaving less,modest around the house),
                _pp(• Add home event 26,• Add night event 8))
            _mcs2(MomTight,
                Tight,
                _pp(Have her start acting much,more comfortably around you,and your sister),
                _pp(• Add home events 27~28,• Add night event 15,• Add mom convo 9))
            _mcs2(MomOral,
                Oral,
                _pp(Have her start providing oral,relief in the mornings along with,your coffee),
                _pp(• Add home event 9))
        }

        {
            node [group="mom_horny"]
            _mcs1(MomHorny,
                Horny,
                _pp(Make her uncontrollably,aroused whenever you&apos;re near,her),
                _pp(• Add mom convo 4,• Remove mom convos 1~2))
            _mcs1(MomTouch,
                Touch,
                _pp(Make it so she can no longer,cum unless you&apos;re in physical,contact with her),
            _pp(• Add home event 5,• Add sister convo 12,• Add mom convo 5))
            _mcs2(MomMasturbate,
                Masturbate,
                _pp(Have her start openly,masturbating around the house),
                _pp(• Add home events 8 and 37,• Add sister convo 15))
            _item(MomGiveStrapon,
                Strange strapon,
                _pp(Use the strange strapon on,mom),
                _pp(• Require to buy the strapon,• Add mom convo 7))
        }

        {
            node [group="mom_maid_collar"]
            _mcs3(MomMaidWithCollar,
                Maid,
                _pp(Have her become your,submissive house maid),
                _pp())
        }

        {
            node [group="mom_hunting"]
            _mcs2(MomHunting,
                Hunting,
                _pp(Have her start going out to find,people to relieve her urges),
                _pp(• Add home events 29 and 34,• Add sister convo 26,• Add mom convo 11))
            _mcs2(MomSlut,
                Slut,
                _pp(Have her start bringing,strangers home in a desperate,effort to get off),
                _pp(• Add home event 11))
            _mcs3(MomFuck,
                Fuck,
                _pp(Have her start hosting,gangbangs and orgies at home),
                _pp(• Add home events 12 and 30,• Add sister convo 28))
            _mcsx(MomBrokenHome,
                BrokenHome,
                _pp(Use the MCS-X on your mom),
                _pp(• Mind break mom and sister too,unless dosed with MCS-X,• Remove all/most of home,events))
        }

        {
            node [group="mom_nude"]
            _mcs2(MomNude,
                Nude,
                _pp(Have her start walking around,the house completely naked),
                _pp(`• Add home events 28, 41 and 43',• Add sister convo 24))
            _mcs2(MomLove,
                Love,
                _pp(Have her start acting more,sexually open around the house),
                _pp(`• Add home events 9, 26 and 40',• Add night event 5,• Add sister convo 25,• Add mom convo 10))
            _mcs3(MomThreesome,
                Threesome,
                _pp(Have her become completely,sexually available to you and,your sister),
                _pp(• Make mom open to threesomes))
            _mcsx(MomTightestFamily,
                Tightest family,
                _pp(Use the MCS-X on your mom),
                _pp(• Loving throuple ending,• Pure vanilla ending))
        }

        {
            node [group="mom_break"]
            _mcs3(MomBreak,
                Break,
                _pp(Have her start thinking and,dreaming about nothing but sex),
                _pp(• Add home events 10 and 32,• Add mom convo 14))
            _mcsx(MomAvaBride,
                Bride,
                _pp(Use the MCS-X on your mom),
                _pp(• Long live the newlyweds!,• Submissive ending,<font color="_sub_color">• MC &rarr; Cat collar TF</font>))
        }

        {
            node [group="mom_lover"]
            _mcs3(MomLover,
                Lover,
                _pp(Have her start thinking of you,as her lover),
                _pp(• Add home events 45 and 47))
            _mcsx(MomMommyBride,
                Mommy bride,
                _pp(Use the MCS-X on your mom),
                _pp(• Long live the newlyweds!,• Pure vanilla ending))
        }

        {
            node [group="mom_whore"]
            _mcs3(MomWhore,
                Whore,
                _pp(Have her let you start setting,her up with people to have sex,with),
                _pp(• Add home event 36,• Add sister convo 32))
            _mcsx(MomSell,
                Sell,
                _pp(Use the MCS-X on your mom),
                _pp(• Sell mom to the The Company,• Could be worse ending))
        }

        {
            node [group="mom_dom"]
            _mcs3(MomDom,
                Dom,
                _pp(Have her become more sure,and assertive in fulfilling her,needs),
                _pp(• MC&apos;s bedroom door lock is no,longer effective))
            _mcsx(MomManiac,
                Maniac,
                _pp(Use the MCS-X on your mom),
                _pp(• Locks sister and MC in cage,• Enslaved ending,<font color="_sub_color">• MC &rarr; Cat collar TF</font>))
        }

        Mom -> MomCoffee
        rank=same { MomGiveCollarReq -> Mom [_lead_to_back(),_below] }
        MomGiveCollarReq -> MomGiveCollarBackfire [_lead_to(if MC is sub)]
        MomCoffee -> MomHorny
        MomCoffee -> MomKiss
        MomHorny -> MomTouch
        MomKiss -> MomUndress
        MomTouch -> MomHunting
        MomTouch -> MomMasturbate
        MomUndress -> MomNude
        MomUndress -> MomTight
        MomHunting -> MomSlut
        MomMasturbate -> MomGiveStrapon
        MomNude -> MomLove
        MomTight -> MomOral
        MomLove -> MomThreesome [label="available only if sister dosed with \"sex\"\nand endings \"dog\" and \"pornstar\"\nhave not been completed"]
        MomLove -> MomDom
        MomLove -> MomLover [_love_link,label="available only if sister ending \"dog\" or\n\"pornstar\" has been completed"]
        MomOral -> MomLover [_love_link]
        MomSlut -> MomFuck
        MomSlut -> MomWhore
        MomGiveStrapon -> MomBreak
        MomGiveStrapon -> MomMaidWithCollar [label="available only if mom\nwears the cat collar"]
        MomBreak -> MomAvaBride
        MomBreak -> MomManiac
        MomDom -> MomManiac
        MomLover -> MomMommyBride [_love_link]
        MomFuck -> MomSell [label="forced if sister is already\ndosed with MCS-X:\n\"dog\" or \"pornstar\""]
        MomFuck -> MomBrokenHome [label="otherwise"]
        MomThreesome -> MomTightestFamily
        MomWhore -> MomSell

        // Better node grouping
        rank=same { MomFuck -> MomWhore [style=invis] }
    }

    subgraph cluster_Penny {
        penwidth=5
        color="_penny_color"
        label=_person_label(
            Penny,
            img/penny.png,
            _penny_color
        )

        _person(Penny,img/penny.png)

        {
            node [group="penny_serum"]
            _mcs2(PennySerumMoney,
                Serum money,
                _pp(Have her start giving you her,share of serum sales),
                _pp(• Add Penny convo event 14,• Benefits are lost if John,becomes her dom))
            _mcs2(PennySerumDiaries,
                Serum diaries,
                _pp(Start running and documenting,various serum experiments))
        }

        {
            node [group="penny_obedient"]
            _mcs1(PennyObey,
                Obedient,
                _pp(Have her obey your commands,above all others),
                _pp(• Prevent John dominating,women at work))
            _mcs1(PennyMakesMaterials_Obedient,
                Make materials,
                _pp(Have her have some materials,prepared every morning),
                _pp(• Grant a random amount of,materials each morning,• Benefits are lost if John,becomes her dom))
            _mcs1(PennyTestSerums,
                Test serums,
                _pp(Ask her to start testing serums,on herself),
                _pp(• Add work events 5~6,• Add Penny convo events 15~16))
            _mcs2(PennyMakeSerums_Obedient,
                Make serums,
                _pp(Have her start preparing,serums overnight for you),
                _pp(• Grant a random serum dose,each morning,• Benefits are lost if John,becomes her dom))
            _mcs2(PennyTestTFM,
                Test TFM,
                _pp(Have her start testing TFM,serums on herself),
                _pp(• Add work event 7,• Add Penny convo events 18~19))
            _mcs2(PennyMouse,
                Mouse,
                _pp(Check on Penny&apos;s TFM testing),
                _pp(<font color="_tf_color">• Penny &rarr; Mouse TF</font>,• Text only))
            _mcsx(PennyFembot,
                Fembot,
                _pp(Give Penny a dose of MCS-X),
                _pp(• Not implemented yet))
        }

        {
            node [group="penny_neutral"]
            _req(PennyDate,
                `_std_color',
                Dating her,
                _pp(• The MC only has to visit her,apartment,• There is a unique opportunity to,breakup with her from chapter 3,by leaving after her rant))
            _mcs3(PennyIncreaseOutput,
                Increase output,
                _pp(Have her find a way to increase,lab production output))
        }

        {
            node [group="penny_slut"]
            _mcs1(PennySlut,
                Slut,
                _pp(Have her start acting sexy,around the lab),
                _pp(• Make it event easier to date her))
            _mcs1(PennyMakesMaterials_Slut,
                Make materials,
                _pp(Have her have some materials,prepared every morning),
                _pp(• Grant a random amount of,materials each morning,• Benefits are lost if John,becomes her dom))
            _mcs1(PennyNeedy,
                Needy,
                _pp(Have her start taking her new,sensuality a little more,seriously))
            _mcs2(PennyMakeSerums_Slut,
                Make serums,
                _pp(Have her start preparing,serums overnight for you),
                _pp(• Grant a random serum dose,each morning,• Benefits are lost if John,becomes her dom))
            _mcs2(PennyDom,
                Dom,
                _pp(Have her get in touch with her,true sexual nature),
                _pp(• Add work events 11~13,• Add Penny convo events 26~28))
            _mcs2(PennySadist,
                Sadist,
                _pp(Have her start embracing her,new dominance),
                _pp(• Add work event 10,• Add Penny convo events 24~25,• Prevent John dominating,women at work))
            _mcsx(PennyMadScientist,
                Mad scientist,
                _pp(Give Penny a dose of MCS-X),
                _pp(• Not implemented yet))
        }

        {
            node [group="penny_whore"]
            _mcs2(PennyOfficeSlut,
                Office slut,
                _pp(Have her start acting slutty,around the whole office),
                _pp(• Add work event 8,• Add Penny convo events 20~21))
            _mcs2(PennyWhore,
                Whore,
                _pp(Have her start whoring herself,`out to the staff, giving you the',profits),
                _pp(• Add work event 9,• Add Penny convo events 22~23))
        }

        Penny -> PennyDate [_below]
        PennyDate -> PennyObey
        PennyDate -> PennySlut
        PennyObey -> PennyMakesMaterials_Obedient
        PennySlut -> PennyMakesMaterials_Slut
        PennyMakesMaterials_Obedient -> PennyTestSerums
        PennyMakesMaterials_Slut -> PennyNeedy
        PennyTestSerums -> PennyMakeSerums_Obedient
        PennyNeedy -> PennyMakeSerums_Slut
        PennyMakeSerums_Obedient -> PennyTestTFM
        PennyMakeSerums_Obedient -> PennySerumMoney
        PennyMakeSerums_Slut -> PennyDom [_sub_link]
        PennyMakeSerums_Slut -> PennyOfficeSlut
        PennyDom -> PennySadist [_sub_link]
        PennyOfficeSlut -> PennyWhore
        PennySerumMoney -> PennySerumDiaries
        PennyTestTFM -> PennyMouse [_tf_link]
        PennyMouse -> PennyIncreaseOutput
        PennySadist -> PennyIncreaseOutput [_sub_link]
        PennySerumDiaries -> PennyIncreaseOutput
        PennyWhore -> PennyIncreaseOutput
        PennyIncreaseOutput -> PennyFembot [label=otherwise]
        PennyIncreaseOutput -> PennyMadScientist [_sub_link,label="forced if: Penny is dom"]
    }

    subgraph cluster_Sister {
        penwidth=5
        color="_sister_color"
        label=_person_label(
            Sister,
            img/sister.png,
            _sister_color
        )

        _person(Sister,img/sister.png)
        _mcs1(SisterPorn,
            Porn,
            _pp(Have her want to start watching,porn with you),
            _pp(• Add home event 15,• Add night event 2))

        {
            node [group="sister_bitch"]
            _mcs3(SisterBitch,
                Bitch,
                _pp(Have her start acting like a,bitch in heat),
                _pp(• Add night event 13,• Remove night event 3))
            _mcsx(SisterDog,
                Dog,
                _pp(Use the MCS-X on your sister),
                _pp(• Act if sister left family,• Remove home events 38~39))
        }

        {
            node [group="sister_slut"]
            _mcs1(SisterTease,
                Tease,
                _pp(Have her start wanting you to,see her naked),
                _pp(• Add night event 1))
            _mcs1(SisterRelief,
                Relief,
                _pp(Have her come to you to,relieve sexual stress),
                _pp(• Add home event 31, Add night event 10,• Remove night event 1))
            _mcs2(SisterOral_Slut,
                Oral,
                _pp(Have her want to engage in oral,sex with you),
                _pp(• Add night event 11))
            _mcs2(SisterSlut,
                Slut,
                _pp(Have her acting more sexually,open with other people),
                _pp(• Add home event 19))
            _mcs3(SisterSex_Slut,
                Sex,
                _pp(Have her want to start have sex,with you),
                _pp(• If the MC is a woman or can use,their penis: add dream event 15,• Otherwise: add dream event 14,and lose dominance))
            _mcs3(SisterSexHouse_Slut,
                Sex house,
                _pp(Have her stop trying to hide,your sexual relationship at,home),
                _pp(• Make sister open to threesomes,• Also open mom if she&apos;s been,dosed with MCS-3 <b>break</b>  or <b>fuck</b>))
            _mcsx(SisterMistress,
                Mistress,
                _pp(Enforce your relationship with,your sister as your mistress),
                _pp())
        }

        {
            node [group="sister_love"]
            _mcs1(SisterMasturbate,
                Masturbate,
                _pp(Have her start masturbating,while you&apos;re watching porn,together),
                _pp())
            _mcs1(SisterKiss,
                Kiss,
                _pp(Have her come to your room,before bed to kiss you,goodnight),
                _pp(• Add night event 9,• Remove sister convo 14))
            _mcs1(SisterHandjob,
                Handjob,
                _pp(Have her want to start touching,each other while watching porn,together),
                _pp())
            _mcs2(SisterOral_Love,
                Oral,
                _pp(Have her want to engage in oral,sex with you),
                _pp(• Add home event 24,• Add night event 4))
            _mcs2(SisterLove,
                Love,
                _pp(Have her want to start exploring,your sibling relationship further),
                _pp(• Add home event 31,• Add night event 12))
            _mcs3(SisterSex_Love,
                Sex,
                _pp(Have her want to start have sex,with you),
                _pp(• If the MC is a woman or can use,their penis: add dream event 15,• Otherwise: add dream event 14,and lose dominance))
            _mcs3(SisterSexHouse_Love,
                Sex house,
                _pp(Have her stop trying to hide,your sexual relationship at,home),
                _pp(• Make sister open to threesomes,• Also open mom if she&apos;s been,dosed with MCS-3 <b>break</b>  or <b>fuck</b>))
            _mcsx(SisterLover,
                Lover,
                _pp(Enforce your relationship with,your sister as your lover),
                _pp())
        }

        {
            node [group="sister_date_camgirl"]
            _req(SisterLoveDate,
                `_std_color',
                Date sister,
                _pp(`At the end of the <b>first</b>  date (and',`only that one) the MC can:',• decide to continue,• decide to put an end to their,relation and breakup with their,sister))
            _req(SisterBreakup,
                `_std_color',
                Breakup,
                _pp(• Removes the love status,• Resets her next dose to MCS-2))
            _mcs3(SisterCamgirl,
                Camgirl,
                _pp(Have her embrace her love of,porn and start camming at,home),
                _pp())
            _mcsx(SisterPornstar,
                Pornstar,
                _pp(Use the MCS-X on your sister),
                _pp(• Act if sister left family,• Remove home events 38~39,• Add home events 19 and 24,• Add night event 4))
        }

        Sister -> SisterPorn
        SisterPorn -> SisterMasturbate
        SisterPorn -> SisterTease
        SisterMasturbate -> SisterKiss
        SisterTease -> SisterKiss
        SisterTease -> SisterRelief [_sub_link]
        SisterKiss -> SisterHandjob
        SisterHandjob -> SisterOral_Love
        SisterRelief -> SisterOral_Slut [_sub_link]
        SisterLoveDate -> SisterBreakup [_lead_to(),_below]
        rank=same { SisterLove -> SisterLoveDate [_lead_to(As long as sister has love effect)] }
        rank=same { SisterOral_Love -> SisterBreakup [_lead_to_back,_below] }
        SisterOral_Love -> SisterLove [_love_link,label=otherwise]
        SisterOral_Love -> SisterSlut [label="forced if: breakup\n(it does not turn\nsister dom though)"]
        SisterOral_Slut -> SisterSlut [_sub_link]
        SisterLove -> SisterSex_Love [_love_link]
        SisterSlut -> SisterSex_Slut [_std_or_sub_link]
        SisterSex_Love -> SisterCamgirl [_love_link]
        SisterSex_Love -> SisterSexHouse_Love [_love_link]
        SisterSex_Slut -> SisterSexHouse_Slut [_std_or_sub_link]
        SisterSex_Slut -> SisterBitch [_std_or_sub_link]
        SisterBitch -> SisterDog [_std_or_sub_link,label="unavailable if\nmom dosed with\nMCS-3 Threesome"]
        SisterBitch -> SisterMistress [_std_or_sub_link]
        SisterCamgirl -> SisterLover [_love_link]
        SisterCamgirl -> SisterPornstar [_love_link,label="unavailable if\nmom dosed with\nMCS-3 Threesome, Dom or Break"]
        SisterSexHouse_Love -> SisterLover [_love_link]
        SisterSexHouse_Slut -> SisterMistress [_std_or_sub_link]
    }

    subgraph cluster_Sophie {
        penwidth=5
        color="_sophie_color"
        label=_person_label(
            Sophie,
            img/sophie.png,
            _sophie_color
        )

        _person(Sophie,img/sophie.png)
        _req(SophieReq1,
            `_std_color',
            Mechanics,
            _pp(`• Unless specified, dosing her',does not consume items,`To see all events, one must:',• Dose her on the first evening,• Dose her when she takes you,out at the steakhouse))
        _req(SophieDomPath,
            `_std_color',
            Domination path,
            _pp())
        _req(SophieFlirtPath,
            `_std_color',
            Flirt path,
            _pp())
        _req(SophieSubPath,
            `_std_color',
            Submission path,
            _pp())
        _mcs1(SophieSub0,
            Lab tour,
            _pp(Have her want to please you,sexually),
            _pp(• Only offered during prologue,• Consume MSC-1))
        _mcs_undefined(SophieSub1,
            At steakhouse,
            _pp(Have her go down on you))
        _mcs1(SophieDom0,
            Lab tour,
            _pp(Have her act flirty with you,wanting to tease you),
            _pp(• Only offered during prologue,• Consume MSC-1))
        _mcs_undefined(SophieDom1,
            At steakhouse,
            _pp(Have her show you her tits))
        _mcs1(SophieFlirt0,
            Lab tour,
            _pp(Have her act more casually,with you),
            _pp(• Only offered during prologue,• Consume MSC-1))
        _mcs_undefined(SophieFlirt1,
            At steakhouse,
            _pp(Allow you to address her more,casually))

        {
            node [group="sophie_sissy_tf"]
            _req(SophieSubReq1,
                _sub_color,
                Start feminizing,
                _pp(`1. Have office sex while being',`caged or a sissy',`2. While visiting HQ, MC must:',• not being close to Tasha,• be caged or a sissy))
            _req(SophieSubReq2,
                _sub_color,
                Follow up feminizing,
                _pp(MC must:,• identify as male,• be caged or a sissy,`1. and not being close to Tasha',`when visiting HQ',`2. when visiting Sophie&apos;s',`apartment'))
            _req(SophieMCSissyTF,
                _sub_color,
                `MC &rarr; Sissy TF',
                _pp(<b>Permanent</b>,,• Lock most physical traits,• Lock identity traits,• Lock haircuts))
        }

        {
            node [group="sophie_sub"]
            _mcs_undefined(SophieSub2,
                At steakhouse,
                _pp(Have her start going down on,you in secret))
            _mcs_undefined(SophieSub3,
                At steakhouse,
                _pp(Have her start going down on,you at the office))
            _mcs_undefined(SophieSub4,
                At steakhouse,
                _pp(Have her want to start having,sex in the office))
            _mcs_undefined(SophieSub5,
                At steakhouse,
                _pp(Have her want to start having,sex in public))
        }

        {
        node [group="sophie_dom"]
            _mcs_undefined(SophieDom2,
                At steakhouse,
                _pp(Have her get off by letting you,touch her))
            _mcs_undefined(SophieDom3,
                At steakhouse,
                _pp(Tell her she should be more,aggressive in her advances))
            _mcs_undefined(SophieDom4,
                At steakhouse,
                _pp(Tell her she should want to start,engaging in oral sex))
            _mcs_undefined(SophieDom5,
                At steakhouse,
                _pp(Tell her she should start calling,you to her office for sex))
        }

        {
            node [group="sophie_flirt"]
            _mcs_undefined(SophieFlirt2,
                At steakhouse,
                _pp(Have her give you her personal,phone number))
            _mcs_undefined(SophieFlirt3,
                At steakhouse,
                _pp(Have her start inviting you into,her office for lunch))
            _mcs_undefined(SophieFlirt4,
                At steakhouse,
                _pp(Have her want to start spending,time with you outside the office))
            _mcs_undefined(SophieFlirt5,
                At steakhouse,
                _pp(Have her want to start showing,you more affection),
                _pp(• Enable the dating flag,• As of current version it seems to,be permanent))
        }

        Sophie -> SophieReq1 [_below]
        SophieReq1 -> SophieDomPath [_below]
        SophieReq1 -> SophieFlirtPath [_below]
        SophieReq1 -> SophieSubPath [_below]
        SophieDomPath -> SophieDom0 [_dom_link]
        SophieDomPath -> SophieDom1 [_dom_link]
        SophieDom0 -> SophieDom2 [_dom_link]
        SophieDom1 -> SophieDom2 [_dom_link]
        SophieDom2 -> SophieDom3 [_dom_link]
        SophieDom3 -> SophieDom4 [_dom_link]
        SophieDom4 -> SophieDom5 [_dom_link]
        SophieFlirtPath -> SophieFlirt0 [_love_link]
        SophieFlirtPath -> SophieFlirt1 [_love_link]
        SophieFlirt0 -> SophieFlirt2 [_love_link]
        SophieFlirt1 -> SophieFlirt2 [_love_link]
        SophieFlirt2 -> SophieFlirt3 [_love_link]
        SophieFlirt3 -> SophieFlirt4 [_love_link]
        SophieFlirt4 -> SophieFlirt5 [_love_link]
        SophieSubPath -> SophieSub0 [_sub_link]
        SophieSubPath -> SophieSub1 [_sub_link]
        SophieSub0 -> SophieSub2 [_sub_link]
        SophieSub1 -> SophieSub2 [_sub_link]
        SophieSub2 -> SophieSub3 [_sub_link]
        SophieSub3 -> SophieSub4 [_sub_link]
        SophieSub4 -> SophieSub5 [_sub_link]
        SophieSub4 -> SophieSubReq1 [_sub_link,_lead_to(may lead to)]
        SophieSubReq1 -> SophieSubReq2 [_sub_link,_lead_to(may lead to)]
        SophieSubReq2 -> SophieMCSissyTF [_sub_link,_lead_to]

        // Put Sub nodes closer to Tasha cluster
        rank=same { SophieSub5 -> SophieSubReq1 [style=invis] }
    }

    subgraph cluster_Tasha {
        penwidth=5
        color="_tasha_color"
        label=_person_label(
            Tasha,
            img/tasha.png,
            _tasha_color
        )

        _person(Tasha,img/tasha.png)

        {
            node [group="tasha_others"]
            _req(TashaTransReject,
                `_std_color',
                Reject her, 
                _pp(`• Tasha&apos;s love -100',`• Soft locked on this dose until',`you accept, or see the work event',`30 to bypass it...'))
            _req(TashaDating,
                `_std_color',
                Date opportunity,
                _pp(Warnings:,• <b>Unique</b>  opportunity,• Dating her <b>prevents</b>  dosing her,,• Accept her invitation to date her,• Refuse to breakup,• Leave office later to postpone,and dose her in the meantime))
        }

        {
            node [group="tasha_top"]
            _req(TashaBypass,
                `_std_color',
                Bypass 1st dose,
                _pp(• Witness work event 30))
            _mcs1(TashaMasturbate,
                Masturbate,
                _pp(Have Tasha develop her,exhibitionist side),
                _pp(• Add work event 14,• Slight change in Tasha&apos;s,morning rountine))
            _mcs1(TashaSaleswoman,
                Saleswoman,
                _pp(Make Tasha more confident),
                _pp(• Add work events 50 and 53))
            _mcs2(TashaOral_Masturbate,
                Oral,
                _pp(Have Tasha want to engage in,oral sex),
                _pp(• Add work events 47 and 49,• Can no longer date her))
            _mcs2(TashaConfidence,
                Confidence,
                _pp(Make her even more sexually,confident in herself),
                _pp(• Add work events 51~52,• Add Tasha convo event 21))
            _mcs2(TashaTopEnergy,
                Top energy,
                _pp(Have her embrace her cock, preferring the top position),
                _pp(• Tasha becomes dominant))
            _mcs3(TashaSleepovers_Dom,
                Sleepovers,
                _pp(Have her want to spend more,time with you outside of work),
                _pp(• Can invite Tasha at home,• Can go to Tasha&apos;apartment))
            _mcs3(TashaAdventure,
                Adventure,
                _pp(Have her want to start acting,more adventurous outside of,work))
            _mcs3(TashaClubbing,
                Clubbing,
                _pp(Have her become even more,involved in the club scene))
            _mcsx(TashaStripper,
                Stripper,
                _pp(Use the MCS-X on Tasha),
                _pp(• Eventually leads to Tasha,leaving the company, ,• Prevent John dose to backfire))
        }

        {
            node [group="tasha_leave_company"]
            _mcsx(TashaBunny,
                Bunny,
                _pp(Use the MCS-X on Tasha),
                _pp(<font color="_tf_color">• Tasha &rarr; Bunny TF</font>,• Text only))
        }

        {
            node [group="tasha_neutral"]
            _mcs1(TashaTransReveal,
                Trans reveal,
                _pp(See what Tasha&apos;s up to and,decide from there))
            _req(TashaTransAccept,
                `_std_color',
                Gender reveal,
                _pp(• Add work event 14,• Add Tasha convo event 4))
        }

        {
            node [group="tasha_vagina_tf"]
            _req(TashaVaginaReq,
                _tf_color,
                Requirements,
                _pp(• Date Tasha,• Invite Tasha at home once to,meet your family,• Wait 3~7 days,• Complete chapter 3))
            _req(TashaVaginaTF,
                _tf_color,
                `Tasha &rarr; Vagina TF',
                _pp(`• Invite Tasha, Saya should',appear,• Discuss the dream at work,`• Wait 2 days, invite Tasha',• Accept Saya&apos;soffer,` ',• Prevent John dose to backfire))
        }

        {
            node [group="tasha_love"]
            _mcs1(TashaCoffee,
                Coffee,
                _pp(Have Tasha start buying you,coffee in the morning),
                _pp(• Add Tasha convo event 11))
            _mcs1(TashaDevoted,
                Devoted,
                _pp(Convince Tasha she may be in,love with you),
                _pp(• Add work event 16,• Add Tasha convo event 12))
            _mcs2(TashaOral_Coffee,
                Oral,
                _pp(Have Tasha want to engage in,oral sex),
                _pp(• Add work events 47 and 49,• Slight changes in Tasha&apos;s,morning routine))
            _mcs2(TashaFriend,
                Friend,
                _pp(Have her want to become even,`more,overtly affectionate with',you),
                _pp(• Add work event 46,• Add Tasha convo event 15))
            _mcs2(TashaAfterHours,
                After hours,
                _pp(Have her stay late to fool,around with you at the office))
            _mcs3(TashaSleepovers_Love,
                Sleepovers,
                _pp(Have her want to spend more,time with you outside of work),
                _pp(• Can invite Tasha at home,• Can go to Tasha&apos;apartment))
            _mcs3(TashaFamily,
                Family,
                _pp(Have her want to become more,involved with you and your,family outside of work))
            _mcs3(TashaWedding,
                Wedding,
                _pp(Have her believe she&apos;s your,wife))
        }

        Tasha -> TashaTransReveal
        Tasha -> TashaBypass [_below]
        rank=same { TashaTransReject -> TashaTransReveal [dir=back] }
        rank=same { TashaDating -> TashaTransAccept [_lead_to_back(Any evening after 14 days)] }
        TashaTransReveal -> TashaTransAccept
        TashaBypass -> TashaTransAccept
        TashaTransAccept -> TashaCoffee
        TashaTransAccept -> TashaMasturbate
        TashaCoffee -> TashaDevoted
        TashaMasturbate -> TashaSaleswoman
        TashaDevoted -> TashaOral_Coffee
        TashaSaleswoman -> TashaOral_Masturbate
        TashaOral_Masturbate -> TashaConfidence
        TashaOral_Coffee -> TashaFriend
        TashaBypass -> JohnOralSlave [_depends_on(requires)]
        TashaConfidence -> TashaTopEnergy [_sub_link]
        TashaFriend -> TashaAfterHours
        SophieSubReq1 -> TashaFriend [_depends_on_back(prevents)]
        SophieSubReq2 -> TashaFriend [_depends_on_back(prevents)]
        TashaAfterHours -> TashaSleepovers_Love [_love_link]
        TashaTopEnergy -> TashaSleepovers_Dom [_sub_link]
        TashaSleepovers_Dom -> TashaAdventure [_sub_link]
        TashaSleepovers_Love -> TashaFamily [_love_link]
        TashaSleepovers_Love -> TashaVaginaReq [_tf_link,_lead_to(),_below]
        TashaAdventure -> TashaClubbing [_sub_link]
        TashaFamily -> TashaWedding [_love_link]
        TashaVaginaReq -> TashaVaginaTF [_tf_link,_lead_to]
        TashaClubbing -> TashaBunny [_tf_link,label="forced if:\nTasha's lust > 49"]
        TashaClubbing -> TashaStripper [_sub_link,label="otherwise"]
    }
}
