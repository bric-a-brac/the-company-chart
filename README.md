# Resources for TheCompany

## Consult resources online

Resources are hosted on [gitlab
pages](https://bric-a-brac.gitlab.io/the-company-chart).

## Rebuild resources locally

### Requirements

The packages needed to rebuild resources locally are listed in the
[Dockerfile](./Dockerfile).

For the flowchart:

* [GNU/m4](https://www.gnu.org/software/m4/)
* [dot](https://graphviz.org/doc/info/lang.html)

For the lab simulator:

* `python` (3+) and `pip`

### Commands

Commands needed to rebuild resources locally are listed in
[.gitlab-ci.yml](./.gitlab-ci.yml).

## Credits

TheCompany is a game by Westane, [official
site](https://thecompanygame.games/).

